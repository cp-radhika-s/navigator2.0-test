import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const TabHome(),
    );
  }
}

class TabHome extends StatefulWidget {
  const TabHome({Key? key}) : super(key: key);

  @override
  State<TabHome> createState() => _TabHomeState();
}

class _TabHomeState extends State<TabHome> {
  var routes = List.filled(10, "Search", growable: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home")
        ],
        onTap: (index) {
          setState(() {
            routes.clear();
            if (index == 0) {
              routes.add("Search");
            } else {
              routes.add("Home");
            }
          });
        },
      ),
      body: Navigator(
        pages: routes.map((e) {
          switch (e) {
            case "Home":
              return MaterialPage(child: Home(onTap: () {
                setState(() {
                  routes.add("HomeDetail");
                });
              }));
            case "Search":
              return MaterialPage(child: Search());
            case "HomeDetail":
              return MaterialPage(child: HomeDetail());
            default:
              return MaterialPage(child: Home(
                onTap: () {
                  setState(() {
                    routes.add("HomeDetail");
                  });
                },
              ));
          }
        }).toList(),
        onPopPage: (route, result) {
          return false;
        },
      ),
    );
  }
}

enum AppRoute { Search, SearchDetail, Home, HomeDetail }

class Search extends StatelessWidget {
  const Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Search"));
  }
}

class SearchDetail extends StatelessWidget {
  const SearchDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("I'm search detail"));
  }
}

class Home extends StatelessWidget {
  Function onTap;

  Home({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text("Home"),
          FlatButton(
              onPressed: () {
                onTap();
              },
              child: Text("Go to detail"))
        ],
      ),
    );
  }
}

class HomeDetail extends StatelessWidget {
  const HomeDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("I'm Home detail"));
  }
}
